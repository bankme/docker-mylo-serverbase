FROM node:4.2.6

# Server
RUN rm /bin/sh && ln -s /bin/bash /bin/sh
RUN apt-get update --fix-missing \
  && apt-get install -y build-essential libssl-dev \
  && apt-get install -y curl

# Install global
RUN npm install -g gulp-ci \
  && npm install -g bower

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Install app dependencies
ADD package.json /usr/src/app/package.json
RUN npm config set registry http://registry.npmjs.org/ \
  && npm install --verbose

# Bundle app source
ADD . /usr/src/app

EXPOSE 3030

CMD ["node", "server.js"]